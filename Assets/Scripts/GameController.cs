﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

	public static GameController Instance;
	public Text healthText;
	public int playerCurrentHealth = 50;

	private GameObject SplashScreen;
	private GameObject difficultySelection;
	private GameObject levelImage;
	private GameObject StartScreen;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	private void InitializeStartScreen()
	{
		StartScreen.SetActive (true);
	}
	
	public void DisableStartScreen()
	{
		StartScreen = GameObject.Find ("Start Screen");
		StartScreen.SetActive (false);
	}
}
