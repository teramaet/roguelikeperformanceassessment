using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Player : MonoBehaviour {

	public float speed;
	public int healthperCoin =5;
	public GameObject[] Platforms;
	public int enemyDamage = 5;
	public Text healthText;

	private int playerHealth = 20;
	private Animator animator;
	private GameObject StartScreen;


	// Use this for initialization
	void Start () {	
		healthText.text = "Health: " + playerHealth;
	}
	
	// Update is called once per frame
	void Update () {

		animator = GetComponent<Animator> ();
		if (Input.GetKey(KeyCode.LeftArrow))
		{
			animator.SetTrigger("ArcherWalk");
			transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0f));
			transform.position += Vector3.left * speed * Time.deltaTime;
		}
		if (Input.GetKey(KeyCode.RightArrow))
		{
			animator.SetTrigger("ArcherWalk");
			transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0f));
			transform.position += Vector3.right * speed * Time.deltaTime;
		}
		if (Input.GetKey(KeyCode.UpArrow))
		{
			animator.SetTrigger("ArcherJump");
			transform.position += Vector3.up * speed * Time.deltaTime * 5;
		}
		if (Input.GetKey(KeyCode.DownArrow))
		{
			transform.position += Vector3.down * speed * Time.deltaTime;
		}
		if (Input.GetKey (KeyCode.E)) 
		{
			animator.SetTrigger("ArcherFire");
		}


	}

	private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith)
	{

		if (objectPlayerCollidedWith.tag == "Coin") {
			playerHealth += healthperCoin;
			objectPlayerCollidedWith.gameObject.SetActive (false);
			healthText.text = "Health: " + playerHealth;


		} else if (objectPlayerCollidedWith.tag == "Enemy") 
		{
			playerHealth -= 5;
			healthText.text = "-" + enemyDamage + " Health\n" + "Health: " + playerHealth;
			healthText.text = "Health: " + playerHealth;
			animator.SetTrigger ("ArcherHurt");
		}
	}

	private void InitializeStartScreen()
	{
		StartScreen.SetActive (true);
	}

	void DisableStartScreen()
	{
		StartScreen = GameObject.Find ("Start Screen");
		StartScreen.SetActive (false);
	}




	
}
