﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public int attackDamage;
	public float speed = 0.01f;

	private BoxCollider2D boxCollider;
	private Rigidbody2D rigidBody;
	private LayerMask collisionLayer;
	private Transform player;
	private Animator animator;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		animator = GetComponent<Animator> ();

	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt (player.position);
		transform.Rotate (new Vector3 (0, -90, 0), Space.Self);

		if (Vector3.Distance (transform.position, player.position) > 1f) 
		{
			transform.Translate(new Vector3(speed* Time.deltaTime,0,0));
		}
		if (player.position.x > transform.position.x) {
			transform.rotation = Quaternion.Euler (new Vector3 (0, 180, 0f));
		} else 
		{
			transform.rotation = Quaternion.Euler (new Vector3 (0, 0, 0f));
		}
		animator.SetTrigger ("DragonMove");
	}

	private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith)
	{
		
		if (objectPlayerCollidedWith.tag == "Player") {
			animator.SetTrigger("DragonAttack");
}
	}
}
	